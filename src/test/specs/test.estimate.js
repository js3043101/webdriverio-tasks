import { expect } from '@wdio/globals'
import { EmailPage, CloudPage, CalculatorPage } from '../pageobjects/index.js';



describe('Compute Engine Estimate', () => {
    it('Send estimate email', async () => {
        
        await CloudPage.open();
        await CloudPage.searchCalculator();
        await CalculatorPage.addCalcEstimate();

       // Confirm There is a line “Total Estimated Cost: USD ${amount} per 1 month” 
        const totalestimated = await CalculatorPage.getEstimateTotal();
        await expect(CalculatorPage.totalEstimatedCost).toHaveText('Total Estimated Cost: USD '+ totalestimated + ' per 1 month')
       
        //generate email
        await EmailPage.openWindow();
        const email = await EmailPage.getEmail(); 

        //go back to calculator to send email
        await CalculatorPage.switchWindows();
        await CalculatorPage.switchFrame();
        await CalculatorPage.sendEstimateEmail(email);
        
        //check email arrived
        await EmailPage.switchWindows();
        await EmailPage.checkNewEmail();

        //compare estimate total with total in email
        const totalEstimateEmail = await EmailPage.getEstimateTotalEmail()
        await expect(totalestimated).toEqual(totalEstimateEmail);

    })
})

