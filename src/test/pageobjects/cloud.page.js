import { $ } from '@wdio/globals'
import Page from './page.js';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class CloudPage extends Page {
    /**
     * define selectors using getter methods
     */
    get searchBar () {
        return $("//div[@class='p1o4Hf']");
    }

    get inputSearch () {
        return $("//input[@placeholder='Search']");
    }

    get searchButton () {
        return $("//i[@aria-label='Search']");
    }

    get calculatorLink () {
        return $(".K5hUy[target='_self'][href='https://cloud.google.com/products/calculator-legacy?hl=es-419']");
    }

    open () {
        return super.open('cloud.google.com/');
    }

    async searchCalculator () {
        await this.searchBar.click();
        await this.inputSearch.setValue('Google Cloud Platform Pricing Calculator');
        await this.searchButton.click();
        await this.calculatorLink.click();
    }
}

export default new CloudPage();
