import EmailPage from './email.page.js'
import CloudPage from './cloud.page.js'
import CalculatorPage from './calculator.page.js'

export { CloudPage, CalculatorPage, EmailPage };