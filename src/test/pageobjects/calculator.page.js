import { $ } from '@wdio/globals'
import Page from './page.js';

class CalculatorPage extends Page {
    /**
     * define selectors using getter methods
     */
    get firstFrame () {
        return $('//*[@src="https://cloud.google.com/frame/products/calculator-legacy/index_d6a98ba38837346d20babc06ff2153b68c2990fa24322fe52c5f83ec3a78c6a0.frame"]');
    }

    get secondFrame () {
        return $("iframe[id='myFrame']");
    }

    get computeEngine () {
        return $("//div[@class='tab-holder compute']");
    }

    get instancesNumber () {
        return $("//input[@id='input_100']");
    }

    get instancesReason () {
        return $("//input[@id='input_101']");
    }

    get operatingSystem () {
        return $("//md-select-value[@id='select_value_label_92']");
    }

    get operatingSystemSelect () {
        return $("//md-option[@id='select_option_102']");
    }

    get model () {
        return $("//md-select-value[@id='select_value_label_93']//span[@class='md-select-icon']");
    }

    get modelSelect() {
        return $("#select_option_115");
    }

    get machineFamily () {
        return $("//md-select-value[@id='select_value_label_94']//span[@class='md-select-icon']");
    }

    get machineFamilySelect () {
        return $("#select_option_119");
    }

    get series () {
        return $("//md-select-value[@id='select_value_label_95']//span[@class='md-select-icon']");
    }

    get seriesSelect () {
        return $("#select_option_224");
    }

    get machineType () {
        return $("//md-select-value[@id='select_value_label_96']//span[@class='md-select-icon']");
    }

    get machineTypeSelect () {
        return $("#select_option_474");
    }

    get addGPUs () {
        return $("//*[@aria-label='Add GPUs']");
    }

    get GPUtype () {
        return $("//md-select[@id='select_510'] ");
    }

    get GPUtypeSelect () {
        return $("#select_option_517");
    }

    get numberGpu () {
        return $("//md-select[@id='select_512']");
    }

    get numberGpuSelect () {
        return $("#select_option_520");
    }

    get localSSD () {
        return $("//md-select-value[@id='select_value_label_468']");
    }

    get localSSDSelect () {
        return $("#select_option_495");
    }

    get datacenter () {
        return $("//md-select-value[@id='select_value_label_98']");
    }

    get datacenterSearch () {
        return $("//input[@id='input_132']");
    }


    get datacenterSelect () {
        return $("//md-option[@id='select_option_268']");
    }


    get commitedUsage () {
        return $("//md-select[@id='select_140']");
    }

    get commitedUsageSelect () {
        return $("#select_option_138");
    }

    get addEstimate () {
        return $("//form[@name='ComputeEngineForm']//button[@type='button'][normalize-space()='Add to Estimate']");
    }

    get totalEstimatedCost () {
        return $("(//b[contains(text(),'Total Estimated Cost:')])[1]");
    }

    get totalCost () {
        return $("//h2[@class='md-flex ng-binding ng-scope']");
    }

    get emailEstimate () {
        return $('//button[@id="Email Estimate"]');
    }

    get emailEstimatePaste () {
        return $('//input[@ng-model="emailQuote.user.email"]');
    }

    get sendEmail () {
        return $( "//button[normalize-space()='Send Email']");
    }
    

    async switchFrame () {
        await browser.switchToFrame(await this.firstFrame);
        await browser.switchToFrame(await this.secondFrame);
    }
  
    async switchWindows() {
        return super.switchWindows('cloud.google.com');
    }
     //switch frames to interact with calculator
    async switchFrame () {
        await browser.switchToFrame(await this.firstFrame);
        await browser.switchToFrame(await this.secondFrame);
    }

     //Click on compute engine
    async selectComputeEngine(){
        await this.computeEngine.click();
    }
        //4 instances
    async selectInstancesNumber(){
        await this.instancesNumber.setValue('4');
    }

    //reason empty
    async selectInstancesReason(){
        await this.instancesReason.setValue('');
    }
    // select operation system: Debian
    async selectOperationSystem(){
        await this.operatingSystem.waitForClickable();
        await this.operatingSystem.click();
        await this.operatingSystemSelect.waitForClickable();
        await this.operatingSystemSelect.click();
    }

    //Provisioning model: Regular
    async selectProvisionModel(){
        await this.model.click();
        await this.modelSelect.waitForDisplayed();
        await this.modelSelect.click();
    }

     //Machine Family: General purpose waitForDisplayed
    async selectMachineFamily(){
        await this.machineFamily.click();
        await this.machineFamilySelect.waitForDisplayed();
        await this.machineFamilySelect.click();
    }

    //Series: N1 
    async selectSeries(){
        await this.series.click();
        await this.seriesSelect.waitForDisplayed()
        await this.seriesSelect.click();
    }

     //Machine type: n1-standard-8 (vCPUs: 8, RAM: 30 GB)
    async selectMachineType(){
        await this.machineType.click();
        await this.machineTypeSelect.waitForDisplayed();
        await this.machineTypeSelect.click();
    }

     //Machine type: n1-standard-8 (vCPUs: 8, RAM: 30 GB)
    async selectMachineType(){
        await this.machineType.click();
        await this.machineTypeSelect.waitForDisplayed();
        await this.machineTypeSelect.click();
    }
    async selectAddGpu(){
        await this.addGPUs.click();
    }

     //GPU type: NVIDIA Tesla V100
    async selectGpuType(){
        await this.GPUtype.click();
        await this.GPUtypeSelect.waitForDisplayed();
        await this.GPUtypeSelect.click();
    }

       //Number of GPUs: 1
    async selectNumberGpus(){
        await this.numberGpu.click();
        await this.numberGpuSelect.waitForDisplayed();
        await this.numberGpuSelect.click(); 
    }

        //Local SSD: 2x375 Gb
    async selectLocalSsd(){
        await this.localSSD.click(); 
        await this.localSSDSelect.waitForDisplayed();
        await this.localSSDSelect.click();
    }

    //Datacenter location: Frankfurt (europe-west3)
    async selectDataCenter(){
        await this.datacenter.click(); 
        await this.datacenterSearch.waitForDisplayed();
        await this.datacenterSearch.click(); 
        await this.datacenterSearch.setValue('Frankfurt');
        await this.datacenterSelect.waitForClickable();
        await this.datacenterSelect.click()
    }

    //Committed usage: 1 Year
    async selectCommittedUsage(){
        await this.commitedUsage.click(); 
        await this.commitedUsageSelect.waitForClickable();
        await this.commitedUsageSelect.click(); 
    }

      //add to estimation
    async addToEstimate(){
        await this.addEstimate.click();
    }
     //select format you want estimation
    async selectEstimateFormat(){
        await this.emailEstimate.click();
    }
    //select all calculator fields in a single function
    async addCalcEstimate(){
        await this.switchFrame();
        await this.selectComputeEngine();
        await this.selectInstancesNumber();
        await this.selectInstancesReason();
        await this.selectOperationSystem();
        await this.selectProvisionModel();
        await this.selectMachineFamily();
        await this.selectSeries();
        await this.selectMachineType();
        await this.selectAddGpu();
        await this.selectGpuType();
        await this.selectNumberGpus(); 
        await this.selectLocalSsd();
        await this.selectDataCenter()
        await this.selectCommittedUsage(); 
        await this.addToEstimate();
        await this.selectEstimateFormat();
    }


    async sendEstimateEmail(email){
        await this.emailEstimatePaste.waitForExist();
        await this.emailEstimatePaste.setValue(email);
        await this.sendEmail.click();
    }


    async seonSystem(){
        
    }


    async seonSystem(){
        
    }


    async getEstimateTotal () {
        const total = await this.totalEstimatedCost.getText();
        //getting total form estimation
        const total2 = await this.totalCost.getText();
        //total usd estimation number value
        const totalEst = await total2.slice(0,-5);
        return totalEst;

   }

}

export default new CalculatorPage();
