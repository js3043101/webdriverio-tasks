import { $ } from '@wdio/globals'
import Page from './page.js';

class EmailPage extends Page {
    /**
     * define selectors using getter methods
     */

    get genEmail () {
        return $("//button[@class='flex h-full w-full cursor-pointer flex-row items-center justify-between rounded-xl bg-white text-gray-80 shadow-sm  px-4 py-3' and contains(., '@')]");
    }


    get emailText () {
        return $('//*[@class="flex h-full w-full max-w-[490px] items-center justify-center rounded-xl lg:w-screen border border-gray-20"]');
    }

    get newEmail () {
        return $("//*[@class='flex h-full border-l-2 border-l-primary w-full flex-col px-4 text-start hover:bg-primary hover:bg-opacity-15 null ']");
    }

    get emailTotalEst () {
        return $('//h2[contains(text(),"Estimated Monthly Cost: USD")]');
    }

    async getEmail(){
        await this.genEmail.waitForExist();
        const email =  await this.emailText.getText();
        return email; 
    }

    async openWindow () {
        return super.openWindow('https://internxt.com/es/temporary-email');
    }

    async switchWindows() {
        return super.switchWindows('internxt.com');
    }
    

    async checkNewEmail() {
        await this.newEmail.waitForClickable({ timeout: 12000 });
        await this.newEmail.click();
}

    async getEstimateTotalEmail () {
        const totalEmailEstimate = await this.emailTotalEst.getText();
        const totalUsdEmail = await totalEmailEstimate.slice(28);
        return totalUsdEmail;

   }
    
}

export default new EmailPage();
